// ref: https://umijs.org/config/
export default {
  treeShaking: true,
  routes: [
    {
      path: '/',
      component: '../layouts/index',
      routes: [
        { path: '/', component: '../pages/index' },
        { path: '/search', component: '../pages/SearchHotel' },
        { path: '/register', component: '../pages/Auth/Register' },
        { path: '/login', component: '../pages/Auth/Login' },
        { path: '/train', component: '../pages/transportation/Train' },
        { path: '/bus', component: '../pages/transportation/Bus' },
        { path: '/plane', component: '../pages/transportation/Plane' },
      ],
    },
  ],
  plugins: [
    // ref: https://umijs.org/plugin/umi-plugin-react.html
    [
      'umi-plugin-react',
      {
        antd: true,
        dva: true,
        dynamicImport: false,
        title: 'hotel-frontend',
        dll: false,

        routes: {
          exclude: [/components\//],
        },
      },
    ],
  ],
};
