import axios from "axios";

export default axios.create({
  baseURL: "https://api.unsplash.com",
  headers: {
    Authorization:
      "Client-ID e5a0d4e3f8a76db1aa1bc16f4fda83f473ec506fd7058379b300850d2bed5dad"
  }
});
