import React from 'react';
import { Header, Grid, Form, Segment, Button, Message, Icon } from 'semantic-ui-react';
import Link from 'umi/link';

class Login extends React.Component {
  render() {
    return (
      <div className="ui container" style={{ marginTop: 30 }}>
        <Grid textAlign="center" verticalAlign="middle" className="app">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h1" icon style={{ color: 'teal' }} textAlign="center">
              <Icon name="puzzle piece" style={{ color: 'teal' }} />
              Login
            </Header>
            <Form>
              <Segment>
                <Form.Input
                  fluid
                  name="email"
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email Address"
                  type="email"
                />
                <Form.Input
                  fluid
                  name="password"
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  type="password"
                />
                <Button color="teal" fluid size="large">
                  Submit
                </Button>
              </Segment>
            </Form>
            <Message>
              Don't have an account? <Link to="/register">Register</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default Login;
