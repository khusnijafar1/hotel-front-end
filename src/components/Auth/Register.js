import React from 'react';
import { FormattedMessage, formatMessage } from 'umi-plugin-react/locale';
import { Header, Grid, Form, Segment, Button, Message, Icon } from 'semantic-ui-react';
import Link from 'umi/link';

class Register extends React.Component {
  state = {
    username: '',
    email: '',
    password: '',
    passwordconfirmation: '',
    errors: [],
    loading: false,
  };

  isFormValid = () => {
    let errors = [];
    let error;

    if (this.isFormEmpty(this.state)) {
      error = { message: 'Fill in all fields' };
      this.setState({ errors: errors.concat(error) });
      return false;
    } else if (!this.isPasswordValid(this.state)) {
      error = { message: 'Password is invalid' };
      this.setState({ errors: errors.concat(error) });
      return false;
    } else {
      return true;
    }
  };

  isFormEmpty = ({ username, email, password, passwordconfirmation }) => {
    return !username.length || !email.length || !password.length || !passwordconfirmation.length;
  };

  isPasswordValid = ({ password, passwordconfirmation }) => {
    if (password.length < 6 || passwordconfirmation.length < 6) {
      return false;
    } else if (password !== passwordconfirmation) {
      return false;
    } else {
      return true;
    }
  };

  displayErrors = errors => errors.map((error, i) => <p key={i}>{error.message}</p>);

  handleChange = event => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    if (this.isFormValid()) {
      this.setState({ errors: [], loading: false });
      window.location.replace('/')
    }
  };

  handleInputError = (errors, inputName) => {
    return errors.some(error => error.message.toLowerCase().includes(inputName)) ? 'error' : '';
  };

  render() {
    const { username, email, password, passwordconfirmation, errors, loading } = this.state;
    return (
      <div className="ui container" style={{ marginTop: 30 }}>
        <Grid textAlign="center" verticalAlign="middle" className="app">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h1" icon style={{ color: 'black' }} textAlign="center">
              <Icon name="jenkins" style={{ color: 'black' }} />
              Register
            </Header>
            <Form onSubmit={this.handleSubmit} size="large">
              <Segment stacked>
                <Form.Input
                  fluid
                  name="username"
                  icon="user"
                  iconPosition="left"
                  placeholder={`${formatMessage({ id: 'user-login.login.username'})}`}
                  onChange={this.handleChange}
                  value={username}
                  required = {`${formatMessage({ id: 'user-login.username.required'})}`}
                  type="text"
                />
                <Form.Input
                  fluid
                  name="email"
                  icon="mail"
                  iconPosition="left"
                  placeholder="Email Address"
                  onChange={this.handleChange}
                  value={email}
                  className={this.handleInputError(errors, 'email')}
                  type="email"
                />
                <Form.Input
                  fluid
                  name="password"
                  icon="lock"
                  iconPosition="left"
                  placeholder="Password"
                  onChange={this.handleChange}
                  value={password}
                  className={this.handleInputError(errors, 'password')}
                  type="password"
                />
                <Form.Input
                  fluid
                  name="passwordconfirmation"
                  icon="repeat"
                  iconPosition="left"
                  placeholder="Password Confirmation"
                  onChange={this.handleChange}
                  value={passwordconfirmation}
                  className={this.handleInputError(errors, 'password')}
                  type="password"
                />
                <Button
                  disabled={loading}
                  className={loading ? 'loading' : ''}
                  color="primary"
                  fluid
                  size="large"
                >
                  Submit
                </Button>
              </Segment>
            </Form>
            {errors.length > 0 && (
              <Message error>
                <h3>Error</h3>
                {this.displayErrors(errors)}
              </Message>
            )}
            <Message>
              Already a user? <Link to="/login">Login</Link>
            </Message>
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default Register;
