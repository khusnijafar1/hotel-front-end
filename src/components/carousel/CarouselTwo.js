import React from 'react';
import Slider from 'react-animated-slider';
import 'react-animated-slider/build/horizontal.css';
import 'normalize.css/normalize.css';
import './slider-animations.css';
import './styles.css';
import Link from 'umi/link';

const content = [
  {
    title: 'Vulputate Mollis Ultricies',
    description:
      'Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Cras justo odio, dapibus ac facilisis.',
    button: 'Discover',
    image:
      'https://www.dsa-arch.com/wp-content/uploads/2017/01/9.-OneOnly-The-Palm-Villa-with-pool-at-dusk-1280x853.jpg',
  },
  {
    title: 'Tortor Dapibus Commodo',
    description:
      'Nullam id dolor id nibh ultricies vehicula ut id elit. Cras mattis consectetur purus sit amet fermentum. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Donec sed odio dui.',
    button: 'Discover',
    image:
      'https://www.dsa-arch.com/wp-content/uploads/2017/01/9.-OneOnly-The-Palm-Villa-with-pool-at-dusk-1280x853.jpg',
  },
  {
    title: 'Phasellus volutpat metus',
    description:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Duis mollis, est non commodo luctus, nisi erat porttitor ligula.',
    button: 'Discover',
    image:
      'https://www.dsa-arch.com/wp-content/uploads/2017/01/9.-OneOnly-The-Palm-Villa-with-pool-at-dusk-1280x853.jpg',
  },
];

const inner = {
  padding: '0 70px',
  boxSizing: 'border-box',
  position: 'absolute',
  width: '100%',
  top: '50%',
  left: '50%',
  WebkitTransform: 'translate(-50%, -50%)',
  transform: 'translate(-50%, -50%)',
};

const header = {
  color: '#FFEA00',
  fontSize: 64,
  lineHeight: 1,
  fontWeight: 900,
  maxWidth: 840,
  margin: '0 auto',
};

const paragraph = {
  color: '#FAFAFA',
  fontSize: 18,
  lineHeight: 1.5,
  margin: '20px auto 30px',
  maxWidth: 640,
  fontWeight: 'bold',
};

const button = {
  marginLeft: 400,
  color: 'black'
};

class CarouselTwo extends React.Component {
  constructor(props) {
      super(props)
    this.routeChange = this.routeChange.bind(this);
  }

  routeChange() {
    let path = '/detail';
    this.props.history.push(path);
  }

  render() {
    return (
      <div>
        <Slider className="slider">
          {content.map((item, index) => (
            <div
              key={index}
              className="slider-content"
              style={{ background: `url('${item.image}') no-repeat center center` }}
            >
              <div className="inner" style={inner}>
                <h1 style={header}>{item.title}</h1>
                <p style={paragraph}>{item.description}</p>
               <Link to="/detail"> <button style={button}>{item.button}</button></Link>
              </div>
            </div>
          ))}
        </Slider>
      </div>
    );
  }
}

export default CarouselTwo;
