import React from 'react';
import { DropDownList } from '@progress/kendo-react-dropdowns';
import { Row, Table, Col, Form } from 'antd';
import hotel from '../../../mock/hotel';
import 'antd/dist/antd.css';
import '@progress/kendo-theme-default/dist/all.css';

const defaultItemSupplier = { hotelSupplierCode: 'Supplier...' };
const defaultItemCountry = { hotelPlaceCountryCode: 'Country...' };
const defaultItemPlace = { hotelPlaceDestinationCode: 'Place...' };

export class CascadingDropdown extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      supplier: null,
      country: null,
      place: null,
      nameHotel: null,
    };
  }

  supplierChange = event => {
    const supplier = event.target.value;
    this.setState({
      supplier: supplier,
      country: null,
      place: null,
    });

    // console.log(this.state);
    hotel.get('/getSupplierCode/' + supplier.hotelSupplierCode).then(response => {
      console.log(response.data);
    });

    hotel.get('/getCountry/' + supplier.hotelSupplierCode).then(response => {
      // console.log(response.data);
      this.setState({
        countries: response.data,
      });
    });
  };

  countryChange = event => {
    const country = event.target.value;
    // console.log('negara', country)
    this.setState({
      country: country,
      place: null,
    });
    hotel
      .get(
        '/getCountryCode/' +
          country.hotelPlaceCountryCode +
          '/' +
          this.state.supplier.hotelSupplierCode,
      )
      .then(response => {
        console.log(response.data);
      });

    hotel
      .get(
        '/getDestination/' +
          country.hotelPlaceCountryCode +
          '/' +
          this.state.supplier.hotelSupplierCode,
      )
      .then(response => {
        // console.log(response.data);
        this.setState({
          places: response.data,
        });
      });
  };

  placeChange = event => {
    const place = event.target.value;
    // console.log(place)
    this.setState({
      place: place,
      nameHotel: null,
    });
    hotel
      .get(
        '/getDestinationCode/' +
          place.hotelPlaceDestinationCode +
          '/' +
          this.state.supplier.hotelSupplierCode +
          '/' +
          this.state.country.hotelPlaceCountryCode,
      )
      .then(response => {
        const hotels = response.data.map(item => {
          return {
            ...item,
            key: item.hotel_code,
          };
        });
        console.log('hotel', hotels);
        this.setState({
          nameHotel: hotels,
        });

        if (hotels.length > 0) {
          let data_match = [];
          for (let i = 0; i < hotels.length; i++) {
            hotel.get('/getMatchHotel/' + hotels[i].bind_id).then(respons => {
              hotels[i].dataMap =
                respons.data
                  .map(item => {
                    if (item.hotel_supplier_code !== this.state.supplier.hotelSupplierCode) {
                      return (
                        item.hotel_supplier_code +
                        ' : ' +
                        item.hotel_name +
                        '(' +
                        item.hotel_code +
                        ')'
                      );
                    }
                  })
                  .join('-') +
                'bind-id = ' +
                hotels[i].bind_id;
              // console.log('data', { data: respons.data });
              data_match.push(respons.data);
              this.setState({
                data_match: respons.data,
              });
            });
          }
        }
      });
  };

  componentDidMount() {
    hotel.get('/getSupplier').then(response => {
      // console.log('www', response.data);
      this.setState({
        supplierCode: response.data,
      });
    });
  }

  render() {
    const { supplier, country, place, nameHotel } = this.state;
    const hasSupplier = supplier && supplier !== defaultItemSupplier;
    const hasCountry = country && country !== defaultItemCountry;

    const columns = [
      {
        title: 'Hotel Info',
        dataIndex: 'hotel_name',
        width: 250,
        sorter: true,
      },
      {
        title: 'Bind Id Matches',
        dataIndex: 'dataMap',
        width: 600,
      },
    ];

    return (
      <div>
        <div style={{ paddingTop: 30, paddingLeft: 50 }}>
          <div style={{ display: 'inline-block' }}>
            Supplier Code :
            <br />
            <DropDownList
              data={this.state.supplierCode}
              textField="hotelSupplierCode"
              onChange={this.supplierChange}
              defaultItem={defaultItemSupplier}
              value={supplier}
              style={{
                border: '1px solid teal',
              }}
            />
          </div>
          <div style={{ display: 'inline-block', marginLeft: 50 }}>
            Country Code :
            <br />
            <DropDownList
              disabled={!hasSupplier}
              data={this.state.countries}
              textField="hotelPlaceCountryCode"
              onChange={this.countryChange}
              defaultItem={defaultItemCountry}
              value={country}
              style={{
                border: '1px solid teal',
              }}
            />
          </div>
          <div style={{ display: 'inline-block', marginLeft: 50 }}>
            Place Code :
            <br />
            <DropDownList
              disabled={!hasCountry}
              data={this.state.places}
              textField="hotelPlaceDestinationCode"
              onChange={this.placeChange}
              defaultItem={defaultItemPlace}
              value={place}
              style={{
                border: '1px solid teal',
              }}
            />
          </div>
        </div>
        <Row>
          <Col span={22} push={1} style={{ marginTop: 60 }}>
            <Table
              columns={columns}
              dataSource={nameHotel}
              pagination={{ pageSize: 50 }}
              scroll={{ y: 300 }}
              bordered
            />
          </Col>
        </Row>
      </div>
    );
  }
}
