import React from 'react';

const Footer = () => {
  const date = document.write(new Date().getFullYear());
  return (
    <div>
      <div className="ui inverted vertical footer segment form-page">
        <div className="ui container">Altomatik 2019 .All Rights Reserved</div>
      </div>
    </div>
  );
};
export default Footer;
