import React from 'react';
import { Menu, Icon, Button } from 'antd';

const { SubMenu } = Menu;

class Header extends React.Component {
  state = {
    collapsed: false,
  };

  toggleCollapsed = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };


  render() {
    return (
      <div>
        <div
          className="ui text menu"
          style={{ backgroundColor: '#00E5FF', boxShadow: 'teal', margin: 0, height: 60 }}
        >
          <a className="item" href="/">
            <img
              src="https://res.cloudinary.com/dwv9umye9/image/upload/v1576481266/logoAltomatik_uapar3.png"
              style={{ height: 40, width: 200, marginLeft: 30 }}
              alt=""
            />
          </a>
          <div className="right menu" style={{ marginRight: 30 }}>
            <a className="ui item" href="/search" style={{ fontWeight: 'bold', padding: 20 }}>
              Find Hotel
            </a>
            <a className="ui item" href="/register" style={{ fontWeight: 'bold', padding: 20 }}>
              Create account
            </a>
            <a className="ui item" href="/login" style={{ fontWeight: 'bold', padding: 20 }}>
              Login
            </a>
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
