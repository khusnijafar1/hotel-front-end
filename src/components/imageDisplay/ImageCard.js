import React from 'react';
import { Rating } from 'semantic-ui-react'

function text(text) {
  if (text != null && text.length > 25) {
    let textSplit = text.substr(0, 40);
    return `${textSplit} ...`;
  } else {
    let textSplit = text;
    return `${textSplit}`;
  }
}

class ImageCard extends React.Component {
  render() {
    const { description, urls } = this.props.image;
    return (
      <div className="column">
        <div className="ui fluid card teal card" style={{ marginTop: 15,marginBottom: 15 }}>
          <div className="image">
            <img ref={this.imageRef} alt={description} src={urls.regular} style={{ height: 200, }} />
            <div className="content" style={{ textAlign: 'center', height: 50 }}>
              <a className="header" href="/detail">{text(description)}</a>
            </div>
          </div>
          <div class="extra">
            Rating :
            <Rating icon="star" defaultRating={4} maxRating={5} />
          </div>
        </div>
      </div>
    );
  }
}

export default ImageCard;
