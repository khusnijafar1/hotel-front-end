import React from 'react';

const PopularList = () => {
  return (
    <div>
      <div className="ui six doubling cards">
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1518612424477-ab93f2480a37?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Jakarta
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Indonesia
            </p>
          </div>
        </div>
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1571048231310-ee1152ec6094?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Bandung
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Indonesia
            </p>
          </div>
        </div>
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1566559532224-6d65e9fc0f37?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Jogjakarta
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Indonesia
            </p>
          </div>
        </div>
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1570477226499-24d15ad36ce7?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Surabaya
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Indonesia
            </p>
          </div>
        </div>
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1499395315858-c4162e2a362f?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Singapore
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Singapore
            </p>
          </div>
        </div>
        <div className="card">
          <div className="image">
            <img
              src="https://images.unsplash.com/photo-1532745609869-16df0d287f3b?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60"
              alt=""
              style={{ height: 150 }}
            />
            <p
              style={{
                position: 'absolute',
                bottom: 30,
                color: '#F9F9FA',
                marginLeft: 20,
                fontWeight: 'bold',
              }}
            >
              Kuala Lumpur
            </p>
            <span />
            <p
              style={{
                position: 'absolute',
                bottom: 20,
                color: '#F9F9FA',
                marginLeft: 20,
              }}
            >
              Malaysia
            </p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PopularList;
