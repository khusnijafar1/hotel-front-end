import React from 'react';

const TextAlignment = () => {
  return (
    <div className="ui four column grid">
      <div className="column">
        <div className="ui fluid card">
          <div className="content">
            <div className="center aligned header" style={{ color: '#1E90FF' }}>
              Clarion Hotel
            </div>
            <div className="center aligned description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="extra content">
            <div className="center aligned author">
              <img
                className="ui avatar image"
                src="https://semantic-ui.com/images/avatar/small/matt.jpg"
              />{' '}
              Thomas
            </div>
          </div>
        </div>
      </div>

      <div className="column">
        <div className="ui fluid card">
          <div className="content">
            <div className="center aligned header" style={{ color: '#1E90FF' }}>
              Grand Hyatt Hotel
            </div>
            <div className="center aligned description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="extra content">
            <div className="center aligned author">
              <img
                className="ui avatar image"
                src="https://semantic-ui.com/images/avatar/small/elliot.jpg"
              />{' '}
              Elliot
            </div>
          </div>
        </div>
      </div>
      <div className="column">
        <div className="ui fluid card">
          <div className="content">
            <div className="center aligned header" style={{ color: '#1E90FF' }}>
              Ibis Hotel
            </div>
            <div className="center aligned description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="extra content">
            <div className="center aligned author">
              <img
                className="ui avatar image"
                src="https://semantic-ui.com/images/avatar/small/steve.jpg"
              />{' '}
              Daniel
            </div>
          </div>
        </div>
      </div>
      <div className="column">
        <div className="ui fluid card">
          <div className="content">
            <div className="center aligned header" style={{ color: '#1E90FF' }}>
              Amaris Hotel
            </div>
            <div className="center aligned description">
              <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>
          </div>
          <div class="extra content">
            <div className="center aligned author">
              <img
                className="ui avatar image"
                src="https://semantic-ui.com/images/avatar/small/stevie.jpg"
              />{' '}
              Ellyse
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default TextAlignment;
