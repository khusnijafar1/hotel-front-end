import React from 'react';
import { Menu, Icon } from 'antd';
import { SubMenu } from 'rc-menu';
import Link from 'umi/link';

class MenuBar extends React.Component {
  state = {
    current: 'promo',
  };

  handleClick = e => {
    console.log('click', e);
    this.setState({
      current: e.key,
    });
  };

  render() {
    return (
      <Menu onClick={this.handleClick} selectedKeys={[this.state.current]} mode="horizontal">
        <Menu.Item key="promo">
          <Icon type="gift" />
          Hotel Promo
        </Menu.Item>
        <SubMenu
          title={
            <span className="submenu-title-wrapper">
              <Icon type="car" />
              Transport
            </span>
          }
        >
          <Menu.ItemGroup title="Darat :">
            <Menu.Item key="transport:1">
              {' '}
              <Link to="/bus">
                <i className="bus icon" />
                Bus
              </Link>
            </Menu.Item>
            <Menu.Item key="transport:2">
              <Link to="/train"><i className="train icon" />
              Train
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>
          <Menu.ItemGroup title="Udara :">
            <Menu.Item key="transport:3">
              <Link to="plane"><i className="plane icon" />
              Plane
              </Link>
            </Menu.Item>
          </Menu.ItemGroup>
        </SubMenu>
        <Menu.Item key="billTopUp">
          <Icon type="dollar" />
          Bills &amp; Top-ups
        </Menu.Item>
      </Menu>
    );
  }
}

export default MenuBar;
