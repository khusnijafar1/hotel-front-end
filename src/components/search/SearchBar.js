import React from 'react';

class SearchBar extends React.Component {
  state = { term: '' };

  onFormSubmit = event => {
    event.preventDefault();

    this.props.onSubmit(this.state.term);
  };
  render() {
    return (
      <div className="ui segment" style={{ border: '1.5px solid teal' }}>
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Hotel Search</label>
            <div className="ui input focus">
              <div className="ui icon input">
                <i className="search icon"></i>
                <input
                  border="1px solid #003580"
                  type="text"
                  placeholder="Type here"
                  value={this.state.term}
                  onChange={e => this.setState({ term: e.target.value })}
                />
              </div>
            </div>
          </div>
        </form>
      </div>
    );
  }
}

export default SearchBar;
