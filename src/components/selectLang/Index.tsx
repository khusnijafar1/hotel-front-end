import { Icon, Menu } from 'antd';
import { formatMessage, getLocale, setLocale } from 'umi-plugin-react/locale';

import { ClickParam } from 'antd/es/menu';
import React from 'react';
import classNames from 'classnames';
import Header from '../header/Header';
import { string } from 'prop-types';
import styles from './index.less';

interface SelectLangProps {
  className?: string;
}

const SelectLang: React.FC<SelectLangProps> = props => {
  const { className } = props;
  const selectedLang = getLocale();
  const changeLang = ({ key }: ClickParam): void => setLocale(key);
  const locales = ['en-US'];
  const languageLabels = {
    'en-US': 'English',
  };
  const languageIcons = {
    'en-US': '🇺🇸',
  };
  const langMenu = <Menu className={styles.menu}></Menu>;
};
