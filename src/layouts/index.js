import Header from '../components/header/Header'
import MenuBar from '../components/menu/MenuBar'

function BasicLayout(props) {
  return (
    <div>
      <Header />
      <MenuBar />
      {props.children}
    </div>
  );
}

export default BasicLayout;
