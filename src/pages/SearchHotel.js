import React from 'react';
import SearchBar from '../components/search/SearchBar';
import ImageList from '../components/imageDisplay/ImageList';
import searchHotel from '../../mock/searchHotel';
import CarouselTwo from '../components/carousel/CarouselTwo';
import PopularList from '../components/imageDisplay/PopularList';
import TextAlignment from '../components/imageDisplay/TextAligment';

class SearchHotel extends React.Component {
  state = { images: [] };

  onSearchSubmit = async term => {
    const response = await searchHotel.get('/search/photos', {
      params: { query: term },
    });

    this.setState({ images: response.data.results });
    console.log(this.state.images);
  };
  render() {
    return (
      <div>
        <div className="ui container" style={{ marginTop: 20 }}>
          <CarouselTwo />
          <h1 style={{ textAlign: 'center' }}>Hello World</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin sit amet metus sit amet
            quam viverra molestie quis a purus. Nunc gravida lacinia urna, eu scelerisque nunc
            scelerisque elementum. Donec id lacus consectetur risus cursus molestie. Nam
            pellentesque aliquet diam, id semper nisi aliquam sit amet. Vivamus sapien neque,
            commodo a dapibus ut, bibendum quis felis. Sed tristique sed enim sed laoreet.
            Suspendisse venenatis et lectus et sodales. Etiam luctus commodo dui, in dictum nisi
            blandit at. Duis a quam tempor, placerat arcu in, eleifend ante. Aenean bibendum, dolor
            at consectetur rutrum, sapien nisi porttitor urna, eu dapibus augue turpis nec lacus.
            Proin tempor arcu eget lacus dapibus euismod. Sed eu enim ante. Suspendisse non libero
            nec nunc vulputate condimentum. Nulla faucibus eu est in ullamcorper. Curabitur
            ullamcorper magna vitae sapien gravida, ut feugiat lectus pharetra. Etiam mauris felis,
            condimentum at pretium et, sollicitudin in mauris.
          </p>
          <SearchBar onSubmit={this.onSearchSubmit} />
          <ImageList images={this.state.images} />
          <div style={{ textAlign: 'center', margin: 40 }}>
            <h1>Overheard From Visitors</h1>
          </div>
          <div style={{ marginTop: 20 }}>
            <TextAlignment />
          </div>
          <div style={{ textAlign: 'center', margin: 40 }}>
            <h1>Most Popular Destinations</h1>
          </div>
          <div style={{ marginBottom: 40 }}>
            <PopularList />
          </div>
        </div>
      </div>
    );
  }
}

export default SearchHotel;
