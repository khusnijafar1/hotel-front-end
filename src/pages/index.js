import { CascadingDropdown } from '../components/dropdownmenu/cascadingDropdown';
import 'semantic-ui-css/semantic.min.css';

export default function() {
  return (
    <div>
      <CascadingDropdown />
    </div>
  );
}
